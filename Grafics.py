import matplotlib.pyplot as plt


def method_graphics(summ, true_value, r, t, s):
    ax = plt.subplot()
    plt.plot(summ, true_value, label="Истинное значение")
    ax.plot(summ, r, "-o", label="Прямоугольники", markeredgewidth=0.05)
    ax.plot(summ, t, "-o", label="Трапеции", markeredgewidth=0.05)
    ax.plot(summ, s, "-o", label="Симпсон", markeredgewidth=0.05)
    plt.xlabel('Кол-во точек')
    plt.ylabel('Значение')
    ax.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
    plt.grid()
    plt.show()


def monte_graphic(points, pi, real_pi):
    plt.plot(points, pi, label='Подсчитанное')
    plt.plot(points, real_pi, label='Реальное')
    plt.title('Изменение значения числа Pi')
    plt.xlabel('Кол-во точек')
    plt.ylabel('Значение Pi')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Значения', title_fontsize='8')
    plt.grid()
    plt.show()


def kr4_graphic(data, sma_data, ema_data, median_data, p):
    plt.plot(p, data, label='Сигнал')
    plt.plot(p, sma_data, label='SMA')
    plt.plot(p, ema_data, label='EMA')
    #plt.plot(p, median_data, label='Median')
    plt.title('')
    plt.xlabel('Кол-во точек')
    plt.ylabel('')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Значения', title_fontsize='8')
    plt.grid()
    plt.show()
