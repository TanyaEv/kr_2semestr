import pandas as pd

data_arr = [elem for elem in pd.read_excel('./data.xls', usecols='M')[12].tolist() if str(elem) != 'nan']


def SMA():
    interval = 40

    points = [i for i in range(len(data_arr))]
    sma = []
    for i in range(interval):
        sma.append(None)

    for i in range(interval, len(data_arr)):
        sma.append((data_arr[i] + data_arr[i - 1] +
                    data_arr[i - 2] + data_arr[i - 3]) / interval)

    return sma, data_arr, points


def EMA(sma):
    interval = 40
    a = 2 / (interval + 1)

    points = [i for i in range(len(data_arr))]
    ema = []
    for i in range(interval):
        ema.append(None)

    ema.append(sma[interval])
    for i in range(interval + 1, len(data_arr)):
        ema.append(a * data_arr[i] + (1 - a) * ema[i - 1])

    return ema, data_arr, points


def Median_filter():
    interval = 40

    points = [i for i in range(len(data_arr))]
    median = []
    for i in range((interval + 1) // 2):
        median.append(None)

    for i in range(len(data_arr) - interval):
        a = [data_arr[i:i + interval]]
        a.sort()
        median.append(a[len(a)//2])

    for i in range((interval + 1) // 2):
        median.append(None)

    return median, data_arr, points
