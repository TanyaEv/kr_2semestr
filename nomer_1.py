from math import sin


def function():
    a, x = 1, 1
    fun = sin(28*a**2 - 27*a*x + 5*x**2)
    return fun


# Метод прямоугольников
def _rectangle_(func, l, r, n_seg, f_rac):
    dx = (r - l) / n_seg  # n_seg - число отрезков на которые разбивается [l:r]

    sum = 0.0
    x_start = (l + f_rac) * dx  # 0 <= f_rac <= 1 задаёт долю смещения точки,
    for i in range(n_seg):  # в которой вычисляется функция,
        sum += func * (x_start + i * dx)  # от левого края отрезка dx

    return sum * dx


# Метод трапеций
def _trapezoid_(func, l, r, n_seg):
    dx = (r - l) / n_seg
    sum = 0.0
    for i in range(1, n_seg):
        sum += func * (l + i * dx)

    return sum * dx


# Метод симпсона
def _simpson_(func, l, r, n_seg):
    if n_seg % 2 == 1:
        n_seg += 1
    dx = (r - l) / n_seg
    sum = 0.0
    for i in range(1, round(n_seg / 2)):
        sum += 2 * func * (l + (2 * i) * dx) + 4 * func * (l + (2 * i + 1) * dx)

    return sum * dx / 3
