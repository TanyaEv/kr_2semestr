from nomer_1 import function, _rectangle_, _trapezoid_, _simpson_
from nomer_3 import Monte_Karlo
from nomer_4 import SMA, EMA, Median_filter
from Grafics import method_graphics, monte_graphic, kr4_graphic

print("Задание\n"
      "[1]Интегрирование\n"
      "[2]Построить графики\n"
      "[3]Метод Монте-Карло\n"
      "[4]Скользящая средняя и медианный фильтр")
num = int(input(">>> "))
l, r = 0, 2

if num == 1:
    n = int(input("Количество разбиений: "))

    rectangle = _rectangle_(function(), l, r, n, 0)
    trapezoid = _trapezoid_(function(), l, r, n)
    simpson = _simpson_(function(), l, r, n)
    print("Полученные результаты:")
    print(f'Метод прямоугольников: {round(rectangle, 3)}\n'
          f'Метод трапеций: {round(trapezoid, 3)}\n'
          f'Метод Симпсона: {round(simpson, 3)}')

elif num == 2:
    n_min = int(input("Минимальное кол-во разбиений: "))
    n_max = int(input("Максимальное кол-во разбиений: "))
    step = 4
    true_value = -0.559  # Истинное значение
    mass_true = [true_value for i in range(n_min, n_max + step, step)]

    n = [i for i in range(n_min, n_max + step, step)]

    rectangle = [_rectangle_(function(), l, r, i, 0) for i in range(n_min, n_max + step, step)]
    trapezoid = [_trapezoid_(function(), l, r, i) for i in range(n_min, n_max + step, step)]
    simpson = [_simpson_(function(), l, r, i) for i in range(n_min, n_max + step, step)]
    all_method = [rectangle, trapezoid, simpson]
    method_graphics(n, mass_true, rectangle, trapezoid, simpson)

elif num == 3:
    points, pi_mass, real_pi_mass = Monte_Karlo(10, 10 ** 4, 200)
    monte_graphic(points, pi_mass, real_pi_mass)

elif num == 4:
    sma_arr, data_arr, points = SMA()
    ema_arr, data, p = EMA(sma_arr)
    median_arr, data_, point = Median_filter()
    kr4_graphic(data_arr, sma_arr, ema_arr, median_arr, points)
